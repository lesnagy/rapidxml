#
# Library configuration file used by dependent projects.
# @file:   rapidxml-config.cmake
# @author: L. Nagy
#

if (NOT DEFINED rapidxml_FOUND)

  # Locate the library headers.
  find_path(rapidxml_include_dir
    NAMES rapidxml.hpp 
          rapidxml_iterators.hpp 
          rapidxml_print.hpp 
          rapidxml_utils.hpp
    PATHS ${rapidxml_DIR}/include
  )

  # Handle REQUIRED, QUIED and version related arguments to find_package(...)
  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(
    rapidxml_DEFAULT_MSG
    rapidxml_include_dir
    rapidxml_libraries
  )

endif()
